### What is this repository for? ###

This project represents a java springboot backend code for the jovio social platform 
* Version :1.0

### How do I get set up? ###

* Clone the repo
* Import it as maven project in eclipse
* Run as spring boot app using your IDE
* By default there is in memory DB configured you can access it http://localhost:8080/h2-console
* Use postman to send rest request against the jovio api

### Contribution guidelines ###

* Jovio team will grant you the access to the repo so that you can clone it and start work on it


### Who do I talk to? ###

* Recruiters
* Contributers