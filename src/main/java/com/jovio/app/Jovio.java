package com.jovio.app;

import java.util.logging.Logger;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Jawed Temori
 */
@SpringBootApplication
public class Jovio implements ApplicationRunner {

	private final static Logger LOGGER = Logger.getLogger(Jovio.class.getName());

	public static void main(String[] args) {
		SpringApplication.run(Jovio.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		LOGGER.info("Jovio started");
	}
}
