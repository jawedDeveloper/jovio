package com.jovio.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.jovio.app.model.Employee;

/**
 * @author Jawed Temori
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	public Employee findByUserName(String name);

	public Employee findByEmailIgnoreCase(String emailId);
}
