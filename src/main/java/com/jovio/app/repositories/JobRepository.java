package com.jovio.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.jovio.app.model.Job;

/**
 * @author Jawed Temori
 */
@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
	public Job findByCompany(String companyName);
}
