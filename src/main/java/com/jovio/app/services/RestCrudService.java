package com.jovio.app.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jovio.app.interfaces.ICrudManager;
import com.jovio.app.model.Employee;
import com.jovio.app.model.Employer;
import com.jovio.app.model.Job;
import com.jovio.app.repositories.EmployeeRepository;
import com.jovio.app.repositories.JobRepository;

/**
 * @author Jawed Temori
 */
@Service
public class RestCrudService implements ICrudManager {
	@Autowired
	EmployeeRepository employRepo;
	@Autowired
	JobRepository jobRepo;

	public Optional<Employee> getById(Long id) {
		return employRepo.findById(id);
	}

	public List<Employee> getAllEmployees() {
		return employRepo.findAll();
	}

	public Employee saveEmployee(Employee theEmployee) {
		theEmployee.getJobList().forEach((job) -> {
			jobRepo.save(job);
		});
		return employRepo.save(theEmployee);
	}

	public String deleteEmployee(Employee theEmployee) {
		employRepo.delete(theEmployee);
		return "Delete OK!";
	}

	// Crud JOBS
	public Optional<Job> getJobById(Long id) {
		return jobRepo.findById(id);
	}

	public List<Job> getAllJobs() {
		return jobRepo.findAll();
	}

	public Job saveJob(Job theJob) {
		return jobRepo.save(theJob);
	}

	public String deleteJob(Job theJob) {
		jobRepo.delete(theJob);
		return "Delete OK!";
	}

	@Override
	public Optional<Employer> getEmployerById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employer> getAllEmployers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employer saveEmployer(Employer theEmployer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteEmployer(Employer theEmployer) {
		// TODO Auto-generated method stub
		return null;
	}

}
