package com.jovio.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.jovio.app.security.ConfirmationToken;

/**
 * @author Jawed Temori
 */
@Entity
public class Employee extends User {

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.REFRESH })
	@JoinTable(name = "employee_jobs", joinColumns = @JoinColumn(name = "empl_id"), inverseJoinColumns = @JoinColumn(name = "job_id"))
	Set<Job> jobList = new HashSet<Job>();

	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.REFRESH }, mappedBy = "employee")
	protected EmployeeProfile profile;

	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.REFRESH }, mappedBy = "employee")
	protected ConfirmationToken confirmationToken;

	public void addJob(Job theJob) {
		jobList.add(theJob);
		theJob.getEmployeeList().add(this);
	}

	public void addProfile(EmployeeProfile thepProfile) {
		this.profile = thepProfile;
		thepProfile.setEmployee(this);
	}

	public Employee() {

	}

	public Employee(String userName, String password, String email, String role) {
		super(userName, password, email, role);
	}

	public EmployeeProfile getProfile() {
		return profile;
	}

	public void setProfile(EmployeeProfile theProfile) {
		if (theProfile != null) {
			this.profile = theProfile;
			theProfile.setEmployee(this);
		}
	}

	public Set<Job> getJobList() {
		return jobList;
	}

	public void setJobList(Set<Job> jobList) {
		this.jobList = jobList;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", password=" + password + ", userName=" + userName + ", email=" + email
				+ ", enabled=" + enabled + ", role=" + role + "]";
	}

}
