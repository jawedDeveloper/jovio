package com.jovio.app.model;

/**
 * @author Jawed Temori
 */
public class Headhunter extends Employer {

	public Headhunter(String companyName, String phoneNumber, String email, String role) {
		super(companyName, phoneNumber, email, role);
	}

}
