package com.jovio.app.model;

/**
 * @author Jawed Temori
 */
public class Company extends Employer {
	public Company(String companyName, String phoneNumber, String email, String role) {
		super(companyName, phoneNumber, email, role);
	}

}
