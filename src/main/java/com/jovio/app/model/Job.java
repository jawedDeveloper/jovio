package com.jovio.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Jawed Temori
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Job {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String title;
	private String description;
	private String company;
	private String recruiterName;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.REFRESH }, mappedBy = "jobList")
	@JsonIgnore
	Set<Employee> employeeList = new HashSet<Employee>();

	@Override
	public String toString() {
		return "Job [id=" + id + ", title=" + title + ", description=" + description + ", company=" + company
				+ ", recruiterName=" + recruiterName + "]";
	}

	public void addEmployee(Employee theEmployee) {
		employeeList.add(theEmployee);
		theEmployee.getJobList().add(this);
	}

	public Job() {

	}

	public Job(String title, String description, String company) {
		this.title = title;
		this.description = description;
		this.company = company;
	}

	public Job(String title, String description, String company, String recruiterName, Set<Employee> employeeList) {
		this.title = title;
		this.description = description;
		this.company = company;
		this.recruiterName = recruiterName;
		this.employeeList = employeeList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getRecruiterName() {
		return recruiterName;
	}

	public void setRecruiterName(String recruiterName) {
		this.recruiterName = recruiterName;
	}

	public Set<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(Set<Employee> employeeList) {
		this.employeeList = employeeList;
	}

}
