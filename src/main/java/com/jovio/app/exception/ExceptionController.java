package com.jovio.app.exception;

import java.util.NoSuchElementException;

import javax.mail.AuthenticationFailedException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Jawed Temori
 */
@ControllerAdvice
public class ExceptionController {
	@ExceptionHandler(value = NoSuchElementException.class)
	public ResponseEntity<Object> resourceNotFound() {
		return new ResponseEntity<Object>("Resource not found", HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = UserExistException.class)
	public ResponseEntity<Object> userNotFound() {
		return new ResponseEntity<Object>("User already exists", HttpStatus.CONFLICT);
	}

	@ExceptionHandler(value = BadCredentialsException.class)
	public ResponseEntity<Object> badCredentials() {
		return new ResponseEntity<Object>("Bad credentials", HttpStatus.CONFLICT);
	}

	@ExceptionHandler(value = AuthenticationException.class)
	public ResponseEntity<Object> authenticationFaild() {
		return new ResponseEntity<Object>("Authentication failed", HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(value = AuthenticationFailedException.class)
	public ResponseEntity<Object> authenticationFaildOnServer() {
		return new ResponseEntity<Object>("Credentials invalid!. Authentication failed on smtp server please inform the admin!", HttpStatus.CONFLICT);
	}
	
}
