package com.jovio.app.controller;

import java.security.SecureRandom;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jovio.app.Jovio;
import com.jovio.app.model.Employee;
import com.jovio.app.model.User;
import com.jovio.app.repositories.ConfirmationTokenRepository;
import com.jovio.app.repositories.EmployeeRepository;
import com.jovio.app.security.AuthenticationRequestDTO;
import com.jovio.app.security.ConfirmationToken;
import com.jovio.app.security.UserService;
import com.jovio.app.services.EmailService;
import com.jovio.app.util.JwtUtil;

/**
 * @author Jawed Temori
 */
@RestController
public class LoginController {
	private final static Logger LOGGER = Logger.getLogger(Jovio.class.getName());

	@Autowired
	private EmployeeRepository userRepository;

	@Autowired
	private ConfirmationTokenRepository confirmationTokenRepository;

	@Autowired
	private EmailService emailSenderService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;

	@Autowired
	private JwtUtil jwtUtil;

	@PostMapping(value = "/authenticate")
	public ResponseEntity<?> authenticateUserAndCreateToken(
			@RequestBody AuthenticationRequestDTO authenticationRequestDTO) throws Exception {
		try {
			BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
				authenticationRequestDTO.getUserName(), bCryptPasswordEncoder.encode(authenticationRequestDTO.getPassword())));
		} catch (AuthenticationException e) {
			throw new Exception("Incorrect username or password", e);
		}
		final UserDetails userDetails = userService.loadUserByUsername(authenticationRequestDTO.getUserName());
		final String jwtToken = jwtUtil.generateToken(userDetails);
		return new ResponseEntity<>(jwtToken, HttpStatus.OK);
	}

	@PostMapping(value = "/register")
	public ResponseEntity<?> registerUser(@RequestBody User theUser) {

		if ("E".equalsIgnoreCase(theUser.getRole())) {
			Employee user = userRepository.findByEmailIgnoreCase(theUser.getEmail());

			if (user != null) {
				return  new ResponseEntity<>("User already exists!", HttpStatus.CONFLICT);
			} else {
				BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());
				Employee employee = new Employee(theUser.getUserName(),
						bCryptPasswordEncoder.encode(theUser.getPassword()), theUser.getEmail(), theUser.getRole());

				userRepository.save(employee);

				ConfirmationToken confirmationToken = new ConfirmationToken(employee);
				confirmationTokenRepository.save(confirmationToken);
				SimpleMailMessage mailMessage = new SimpleMailMessage();
				mailMessage.setTo(theUser.getEmail());
				mailMessage.setSubject("Complete Registration!");
				mailMessage.setFrom("temjawed@gmail.com");
				mailMessage.setText("To confirm your account, please click here : "
						+ "http://localhost:8080/confirm-account?token=" + confirmationToken.getConfirmationToken());

				emailSenderService.sendEmail(mailMessage);
				return new ResponseEntity<>("Registration Email send!", HttpStatus.OK);
			}
		}

		return new ResponseEntity<>("Registration Email could not be sent!" + theUser.getEmail(), HttpStatus.CONFLICT);
	}

	@RequestMapping(value = "/confirm-account", method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<?> confirmUserAccount(@RequestParam("token") String confirmationToken) {
		ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
		Employee user = userRepository.findByEmailIgnoreCase(((Employee) token.getUser()).getEmail());
		if (token != null) {
			user.setEnabled(true);
			userRepository.save(user);
			LOGGER.info("SAVED AND REGISTERED USER!");
			return new ResponseEntity<>("Registration succsessfull!", HttpStatus.OK);
		}
		userRepository.delete(user);
		return new ResponseEntity<>("Registration faild, token invalid!", HttpStatus.CONFLICT);
	}

}
