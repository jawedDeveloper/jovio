package com.jovio.app.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jovio.app.dtos.EmployeeDTO;
import com.jovio.app.model.Employee;
import com.jovio.app.model.Job;
import com.jovio.app.services.RestCrudService;

/**
 * @author Jawed Temori
 */
@RestController
public class RestCrudController {

	@Autowired
	RestCrudService crudService;

	@GetMapping(value = "/employees")
	public List<Employee> getAllEmployees() {
		return crudService.getAllEmployees();
	}

	@PostMapping(value = "/employees")
	public Employee saveEmployee(@RequestBody Employee theEmployee) {
		return crudService.saveEmployee(theEmployee);
	}

	@PutMapping(value = "/employees")
	public Employee updatEmployee(@RequestBody Employee theEmployee) {
		return crudService.saveEmployee(theEmployee);
	}

	@GetMapping(value = "/employees/{id}")
	public EmployeeDTO getEmployeeById(@PathVariable("id") Long id) {
		Employee employee = crudService.getById(id).get();
		return new ModelMapper().map(employee, EmployeeDTO.class);
	}

	@DeleteMapping(value = "/employees")
	public String deletEmployee(@RequestBody Employee theEmployee) {
		return crudService.deleteEmployee(theEmployee);
	}

	// Job Crud

	@GetMapping(value = "/jobs")
	public List<Job> getAllJobs() {
		return crudService.getAllJobs();
	}

	@PostMapping(value = "/jobs")
	public Job saveEmployee(@RequestBody Job theJob) {
		return crudService.saveJob(theJob);
	}

	@PutMapping(value = "/jobs")
	public Job updateJob(@RequestBody Job theJob) {
		return crudService.saveJob(theJob);
	}

	@GetMapping(value = "/jobs/{id}")
	public Job getJobById(@PathVariable("id") Long id) {
		return crudService.getJobById(id).get();
	}

	@DeleteMapping(value = "/jobs")
	public String deleteJob(@RequestBody Job theJob) {
		return crudService.deleteJob(theJob);
	}

}
