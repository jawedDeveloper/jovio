package com.jovio.app.security;
/**
 * @author Jawed Temori
 */
public class AuthenticationRequestDTO {
	
	protected String password;
	protected String userName;
	
	public AuthenticationRequestDTO(String password, String userName) {
		super();
		this.password = password;
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
