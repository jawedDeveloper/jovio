package com.jovio.app.security;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.jovio.app.model.Employee;
import com.jovio.app.repositories.EmployeeRepository;

/**
 * @author Jawed Temori
 */
@Service
public class UserService implements UserDetailsService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// load user from db
		Employee employee = employeeRepository.findByUserName(username);
		if (employee == null) {
			throw new UsernameNotFoundException(username);
		}
		List<GrantedAuthority> roleList = new ArrayList<>();
		roleList.add(new GrantedAuthority() {
			@Override
			public String getAuthority() {
				// TODO Auto-generated method stub
				return employee.getRole();
			}
		});

		return new User(employee.getUserName(), employee.getPassword(), roleList);
	}

}
