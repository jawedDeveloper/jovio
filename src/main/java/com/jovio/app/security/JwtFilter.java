package com.jovio.app.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.jovio.app.util.JwtUtil;

import io.jsonwebtoken.JwtException;

/**
 * @author Jawed Temori
 */
@Component
public class JwtFilter extends OncePerRequestFilter {
	private final static Logger LOGGER = Logger.getLogger(JwtFilter.class.getName());

	@Autowired
	JwtUtil jwtUtil;
	@Autowired
	UserService userService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, JwtException, IOException {
		String header = request.getHeader("Authorization");

		if (header == null || !header.startsWith("Bearer ")) {
			// todo handle the exceptions
			throw new JwtException("No JWT token found in request headers");
		}

		String token = header.substring(7);
		String userName = jwtUtil.extractUserName(token);

		// check there is no other user in the context
		if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {

			if (jwtUtil.validateToken(token, userService.loadUserByUsername(userName))) {

				UserDetails userDetails = userService.loadUserByUsername(userName);
				// create the user for the context
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) request));

				// set the context with authenticated user
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			} else {
				throw new JwtException("Token invalid!");
			}

		}

		filterChain.doFilter(request, response);
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.add("/register");
		excludeUrlPatterns.add("/authenticate");
		excludeUrlPatterns.add("/confirm-account");
		excludeUrlPatterns.add("/h2-console/");
		return excludeUrlPatterns.stream().anyMatch(p -> p.contains(request.getServletPath()));

	}

}
