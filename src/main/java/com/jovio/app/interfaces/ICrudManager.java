package com.jovio.app.interfaces;
import java.util.List;
import java.util.Optional;
import com.jovio.app.model.Employee;
import com.jovio.app.model.Employer;
import com.jovio.app.model.Job;

public interface ICrudManager {
	//Crud Employee
	public Optional<Employee> getById(Long id);
	public  List<Employee> getAllEmployees();
	public  Employee saveEmployee(Employee theEmployee);	
	public  String deleteEmployee(Employee theEmployee);
	
	//Crud Employer
	public Optional<Employer> getEmployerById(Long id);
	public  List<Employer> getAllEmployers();
	public  Employer saveEmployer(Employer theEmployer);	
	public  String deleteEmployer(Employer theEmployer);
	
	//Crud JOBS
	public Optional<Job> getJobById(Long id);	
	public  List<Job> getAllJobs();
	public  Job saveJob(Job theJob);
	public  String deleteJob(Job theJob);

}
