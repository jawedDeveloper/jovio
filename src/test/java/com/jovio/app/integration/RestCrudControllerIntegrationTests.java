package com.jovio.app.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.NoSuchElementException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jovio.app.dtos.EmployeeDTO;
import com.jovio.app.model.Employee;
import com.jovio.app.model.Job;

/**
 * @author Jawed Temori
 */
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(addFilters = false)
public class RestCrudControllerIntegrationTests {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	ObjectMapper objectMapper;

	@Test
	public void shouldListenToRequests() throws Exception {
		mockMvc.perform(get("/employees").contentType("application/json")).andExpect(status().isOk());
	}

	@Test
	public void shouldCreateAnEmployeeAndReturnStatusOK() throws Exception {
		Employee alex = new Employee("Alex", "123", "alex@gmail.com", "E");
		alex.setProfile(null);

		// check the status is ok
		mockMvc.perform(
				post("/employees").contentType("application/json").content(objectMapper.writeValueAsString(alex)))
				.andExpect(status().isOk());

		// check result
		MvcResult result = mockMvc.perform(get("/employees").contentType("application/json")).andReturn();
		String resultString = result.getResponse().getContentAsString();
		Employee[] employeesList = objectMapper.readValue(resultString, Employee[].class);
		assertThat(employeesList.length).isEqualTo(1);

	}

	@Test
	public void shouldGetAllEmployees() throws Exception {

		// create emplyee
		Employee alex = new Employee("sami", "123", "alex@gamil.com", "E");
		Employee employee = (Employee) saveObjectToDB(alex);

		// check result
		MvcResult result = mockMvc.perform(get("/employees").contentType("application/json")).andReturn();
		String resultString = result.getResponse().getContentAsString();
		Employee[] employeesList = objectMapper.readValue(resultString, Employee[].class);
		assertThat(employeesList.length).isEqualTo(1);
		Employee employee1 = (Employee) getObjectById("/employees/", "1", new Employee());
		assertThat(employee.getUserName()).isEqualTo("sami");

	}

	@Test
	public void shouldDeleteEmployee() throws Exception {

		Employee alex = new Employee("aron", "123", "aron@gmail.com", "E");
		Employee employeeToDelele = (Employee) saveObjectToDB(alex);
		assertThat(employeeToDelele.getId()).isEqualTo(1);

		// check the status is ok
		MvcResult result = mockMvc
				.perform(delete("/employees").contentType("application/json")
						.content(objectMapper.writeValueAsString(employeeToDelele)))
				.andExpect(status().isOk()).andReturn();
		String resultString = result.getResponse().getContentAsString();
		assertThat(resultString).isEqualTo("Delete OK!");

	}

	@Test
	public void shouldThrowRecourseNotFoundExceptioin() throws Exception {

		Employee alex = new Employee("alex", "123", "aron@gmail.com", "E");
		Employee employee = (Employee) saveObjectToDB(alex);
		assertThat(employee.getId()).isEqualTo(1);

		MvcResult result2 = mockMvc.perform(get("/employees/2").contentType("application/json")).andReturn();
		NoSuchElementException noSuchElementException = (NoSuchElementException) result2.getResolvedException();
		assertNotNull(noSuchElementException);

	}

	@Test
	public void shouldSaveJobsToEmployee_AndDele() throws Exception {
		// check result
		Employee alex = new Employee("alex", "123", "aron@gmail.com", "E");
		Job javaDeveloperJob = new Job("javaDeveloper", "Develop Java Apps", "BMW");
		alex.addJob(javaDeveloperJob);
		Employee employee = (Employee) saveObjectToDB(alex);
		assertThat(employee.getId()).isEqualTo(1);

		EmployeeDTO alexRetrieved = (EmployeeDTO) getObjectById("/employees/", "1", new EmployeeDTO());
		assertEquals(alexRetrieved.getName(), "alex");

		Job javaDeveloper = (Job) getObjectById("/jobs/", "1", new Job());
		assertEquals(javaDeveloper.getCompany(), "BMW");

		// delete employee
		deleteObject("/employees", alexRetrieved);
		// check employe is not any more in the db
		MvcResult result2 = mockMvc.perform(get("/employees/1").contentType("application/json")).andReturn();
		NoSuchElementException noSuchElementException = (NoSuchElementException) result2.getResolvedException();
		assertNotNull(noSuchElementException);
		// but the job is not deleted job will be deleted ones the offerer of the job i
		// deleted
		Job javaDeveloperRetrieved = (Job) getObjectById("/jobs/", "1", new Job());
		assertEquals(javaDeveloperRetrieved.getCompany(), "BMW");

	}

	private Object saveObjectToDB(Object obj) throws JsonProcessingException, Exception {
		MvcResult result = mockMvc.perform(
				post("/employees").contentType("application/json").content(objectMapper.writeValueAsString(obj)))
				.andReturn();
		String resultString = result.getResponse().getContentAsString();
		return objectMapper.readValue(resultString, obj.getClass());

	}

	private Object getObjectById(String baseUri, String id, Object obj) throws JsonProcessingException, Exception {
		MvcResult result = mockMvc.perform(get(baseUri + id).contentType("application/json")).andReturn();
		String resultString = result.getResponse().getContentAsString();
		return objectMapper.readValue(resultString, obj.getClass());

	}

	private MvcResult deleteObject(String baseUri, Object obj) throws JsonProcessingException, Exception {
		return mockMvc
				.perform(delete(baseUri).contentType("application/json").content(objectMapper.writeValueAsString(obj)))
				.andReturn();

	}
}
