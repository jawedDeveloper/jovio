package com.jovio.app.repositories;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import com.jovio.app.model.Job;

/**
 * @author Jawed Temori
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class JobRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private JobRepository jobRepository;

	@Test
	public void shouldSaveAndRetrievJob() {
		Job javaDeveloper = new Job("JavaDeveloper", "Develop Java Apps", "BMW");
		entityManager.persist(javaDeveloper);
		entityManager.flush();
		Job found = jobRepository.findById(javaDeveloper.getId()).get();
		assertEquals(found.getCompany(), "BMW");
		assertEquals(found.getTitle(), "JavaDeveloper");
	}

}
