package com.jovio.app.repositories;

import static org.junit.Assert.assertEquals;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.jovio.app.model.Employee;
import com.jovio.app.model.Job;

/**
 * @author Jawed Temori
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class JobEmployeeCrudTests {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private EmployeeRepository employeeRepository;

	@Test
	public void shouldSaveEmployeeAndHisJobs() {
		Job javaDeveloper = new Job("JavaDeveloper", "Develop Java Apps", "BMW");
		Job angularDevelober = new Job("AngularDeveloper", "Develop Angular Apps", "Daimler");
		Employee alex = new Employee("Alex", "123", "alex@gmail.com", "E");
		alex.addJob(javaDeveloper);
		alex.addJob(angularDevelober);
		entityManager.persist(alex);
		entityManager.flush();

		Employee foundEmployee = employeeRepository.findByUserName(alex.getUserName());
		assertEquals(foundEmployee.getJobList().toArray().length, 2);

		Job foundJob = jobRepository.findByCompany(javaDeveloper.getCompany());
		assertEquals(foundJob.getCompany(), "BMW");
		assertEquals(foundJob.getEmployeeList().toArray().length, 1);
	}

}
