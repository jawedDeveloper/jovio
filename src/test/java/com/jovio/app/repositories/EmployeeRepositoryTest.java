package com.jovio.app.repositories;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.jovio.app.model.Employee;

/**
 * @author Jawed Temori
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryTest {
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private EmployeeRepository employeeRepository;

	@Test
	public void shouldSaveAndRetrievEmployee() {
		Employee alex = new Employee("alex", "123", "alex@gmx.de", "user");
		entityManager.persist(alex);
		entityManager.flush();
		Employee found = employeeRepository.findByUserName(alex.getUserName());
		assertEquals(found.getUserName(), alex.getUserName());
	}
}
